from django import forms

from .models import Comment
from .models import Post, Category

choices = Category.objects.all().values_list('name','name')

class CommentForm (forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['name', 'email', 'content']



class PostForm (forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'slug', 'category', 'preview', 'content']

        widgets = {
            'category': forms.Select(choices=choices, attrs={'class': 'form-control'})
        }

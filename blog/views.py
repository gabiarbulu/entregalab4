from django.shortcuts import render, redirect
from .models import Post
from .forms import CommentForm, PostForm
# Create your views here.


def frontpage(request):
    posts = Post.objects.all()

    if request.method == 'POST':
        form = PostForm(request.POST)

        if form.is_valid():
            post = form.save(commit=False)
            post.post = post
            post.save()

            return redirect('frontpage')
    else:
        form = PostForm()

    return render(request, 'blog/frontpage.html', {'posts': posts, 'form': form})


def post_info(request, slug):
    post = Post.objects.get(slug=slug)

    if request.method == 'POST':
        form = CommentForm(request.POST)

        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()

            return redirect('post_info', slug=post.slug)
    else:
        form = CommentForm()

    return render(request, 'blog/post_info.html', {'post': post, 'form': form})


def edit_post(request, slug):
    post = Post.objects.get(slug=slug)
    form = PostForm(request.POST or None, instance=post)
    print("oi")
    if form.is_valid():
        form.save()
        return redirect('frontpage')

    return render(request, 'blog/edit_post.html', {'form': form, 'post': post})


def delete_post(request, slug):
    post = Post.objects.get(slug=slug)

    if request.method == 'POST':
        post.delete()
        return redirect('frontpage')

    return render(request, 'blog/delete_post.html', {'post': post})
